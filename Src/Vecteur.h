#ifndef SRC_VECTEUR_H

#define SRC_VECTEUR_H

#include <iostream>

class Vecteur {
 public:
    float DX;
    float DY;
    float DZ;

    Vecteur(float dx = 0.0, float dy = 0.0, float dz = 0.0) {
        DX = dx;
        DY = dy;
        DZ = dz;
    }

    void Print() {
        std::cout << "(" << DX << "," << DY << "," << DZ << ")";
    }
};

#endif  // SRC_VECTEUR_H
